# atlpaste

`atlpaste`, similar to Mac OS X's `pbcopy`, takes stdin and pastes its
contents to https://paste.atlassian.com, placing the resulting URL in
your clipboard.

Install by sourcing `atlpaste.sh` in your profile.
