atlpaste() {
  curl --request POST https://paste.atlassian.com/add \
       --data format=text --data-urlencode content="$(cat)" \
       --data submit='Paste!' \
       --silent \
    | sed 's!.*/view/\([0-9]*\).*!https://paste.atlassian.com/plain/\1!' \
    | awk '{printf $0}' \
    | pbcopy
}
